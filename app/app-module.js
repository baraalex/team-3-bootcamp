(function () {
  'use strict';

  angular
    .module('notificationsApp', ['ui.bootstrap', 'ngRoute', 'topicControllers',
    'topicServices', 'userControllers', 'userServices',
    'notificationControllers', 'notificationServices'])
    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.
        when('/topics', {
          templateUrl: 'home/views/topic.tpl.html',
          controller: 'TopicListCtrl',
          controllerAs: 'topicCtrl'
        }).
        when('/notifications', {
          templateUrl: 'home/views/notification.tpl.html',
          controller: 'NotificationListCtrl',
          controllerAs: 'notificationCtrl'
        }).
      otherwise({
        redirectTo: '/topics'
      });
    }])
    .config(['$compileProvider',
      function ($compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);
      }]);
}());
