angular
  .module('CommonServices', [])
  .factory('BlobEncapsulation', [function () {

    var createBlob = function(data) {
      return new Blob ([data], {type: 'text/csv;charset=utf-8'});
    };

    return {
      createBlob: createBlob
    };
  }]);