angular
  .module('userServices', ['ngResource'])
  .factory('User', ['$resource', function ($resource) {
    var getUsers;
    getUsers = function () {
      return $resource('/notif/admin/equipo3/users', {}, {
        query: {method: 'GET', isArray: true}
      });
    };

    return {
      getUsers: getUsers
    };
  }]);
