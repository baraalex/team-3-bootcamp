angular
  .module('notificationServices', ['ngResource'])
  .factory('Notification', ['$resource', function ($resource) {
    var getNotifications, sendToUsers, deleteNotification;
    getNotifications = function () {
      return $resource('/notif/admin/equipo3/notifications/pending', {}, {
        query: {method: 'GET'}
      });
    };

    sendToUsers = function () {
      return $resource('/notif/admin/equipo3/notifications', {}, {
        save: {method: 'POST',
          interceptor: {
            response: function (response) {
              var result = response.resource;
              result.$status = response.status;
              return result;
            }
          }
        }
      });
    };

    deleteNotification = function (id) {
      return $resource('/notif/admin/equipo3/notifications/' + id, {}, {
        delete: {method: 'DELETE',
          interceptor: {
            response: function (response) {
              var result = response.resource;
              result.$status = response.status;
              return result;
            }
          }}
      });
    };

    return {
      getNotifications: getNotifications,
      sendToUsers: sendToUsers,
      deleteNotification: deleteNotification
    };
  }]);
