angular
  .module('topicServices', ['ngResource'])
  .factory('Topic', ['$resource', function ($resource) {
    var getTopics, createTopic, deleteTopic;

    getTopics = function (number) {
      var num = angular.isDefined(number) && number > 0 ? '?size=' + number : '';
      return $resource('/notif/admin/equipo3/topics' + num, {}, {
        query: {method: 'GET',
          interceptor: {
            response: function (response) {
              var result = response.resource;
              result.$status = response.status;
              return result;
            }
          }}
      });
    };

    createTopic = function () {
      return $resource('/notif/admin/equipo3/topics', {}, {
        save: {method: 'POST',
        interceptor: {
          response: function (response) {
            var result = response.resource;
            result.$status = response.status;
            return result;
          }
        }}
      });
    };

    deleteTopic = function (id) {
      return $resource('/notif/admin/equipo3/topics/' + id, {}, {
        delete: {method: 'DELETE',
          interceptor: {
            response: function (response) {
              var result = response.resource;
              result.$status = response.status;
              return result;
            }
          }}
      });
    };

    return {
      getTopics: getTopics,
      createTopic: createTopic,
      deleteTopic: deleteTopic
    };
  }]);
