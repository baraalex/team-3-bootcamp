/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('Topic controllers', function () {
  beforeEach(function () {
    this.addMatchers({
      toEqualData: function (expected) {
        return angular.equals(this.actual, expected);
      }
    });
  });

  beforeEach(module('notificationsApp'));
  beforeEach(module('topicServices'));

  describe('TopicListCtrl test devolviendo bien la llamada de listar topics', function () {
    var scope, ctrl, $httpBackend;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('TopicListCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 topics', function () {
      expect(ctrl.topics).toEqualData(undefined);
      expect(ctrl.alerts).toEqualData([]);

      $httpBackend.flush();

      expect(ctrl.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('TopicListCtrl test fallando en la 1ª llamada de listar topics', function () {
    var scope, ctrl, $httpBackend;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [204, {}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('TopicListCtrl', {$scope: scope});
    }));

    it('debe tener el objeto de la lista como "undefined" y un alert', function () {
      expect(ctrl.topics).toEqualData(undefined);
      expect(ctrl.alerts).toEqualData([]);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'danger',
      msg: 'No se pueden recuperar los topics del servidor.\nProfavor intentalo mas tarde'}]);

      expect(ctrl.topics).toEqualData(undefined);
    });

    it('debe tener el objeto de la lista como "undefined" y cerrar el alert', function () {
      expect(ctrl.topics).toEqualData(undefined);
      expect(ctrl.alerts).toEqualData([]);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'danger',
      msg: 'No se pueden recuperar los topics del servidor.\nProfavor intentalo mas tarde'}]);

      expect(ctrl.topics).toEqualData(undefined);

      ctrl.closeAlert(0);

      expect(ctrl.alerts).toEqualData([]);
    });
  });

  describe('TopicListCtrl test fallando en la 2ª llamada de listar topics', function () {
    var scope, ctrl, $httpBackend;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 3}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics?size=3')
      .respond(function () {
        return [210, {}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('TopicListCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 0 topics', function () {
      expect(ctrl.topics).toEqualData(undefined);
      expect(ctrl.alerts).toEqualData([]);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'danger',
      msg: 'No se pueden recuperar los topics del servidor.\nProfavor intentalo mas tarde'}]);

      expect(ctrl.topics).toEqualData('');
    });
  });

  describe('TopicListCtrl test devolviendo bien la llamada de listar topics con 2 llamadas (mas elementos que en la 1ª llamada)', function () {
    var scope, ctrl, $httpBackend;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 3}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics?size=3')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('TopicListCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 topics', function () {
      expect(ctrl.topics).toEqualData(undefined);
      expect(ctrl.alerts).toEqualData([]);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([]);

      expect(ctrl.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('TopicListCtrl test delete ok del servidor', function () {
    var scope, ctrl, $httpBackend;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.expect('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
          {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('TopicListCtrl', {$scope: scope});
    }));

    it('debe tener el objeto de la lista con 1 topic y un alert', function () {
      expect(ctrl.topics).toEqualData(undefined);
      expect(ctrl.alerts).toEqualData([]);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([]);

      expect(ctrl.topics).toEqualData([
        {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
        {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      $httpBackend.resetExpectations();

      $httpBackend.expect('DELETE', '/notif/admin/equipo3/topics/1')
      .respond(function () {
        return [204, {}];
      });

      $httpBackend.expect('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 1, totalElements: 1}];
      });

      ctrl.delete(1);
      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'warning', msg: 'Topic borrado'}]);

      expect(ctrl.topics).toEqualData([
        {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('TopicListCtrl test delete no hecho del servidor', function () {
    var scope, ctrl, $httpBackend;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.expect('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
          {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('TopicListCtrl', {$scope: scope});
    }));

    it('debe tener el objeto de la lista con 1 topic y un alert', function () {
      expect(ctrl.topics).toEqualData(undefined);
      expect(ctrl.alerts).toEqualData([]);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([]);

      expect(ctrl.topics).toEqualData([
        {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
        {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      $httpBackend.resetExpectations();

      $httpBackend.expect('DELETE', '/notif/admin/equipo3/topics/1')
      .respond(function () {
        return [200, {}];
      });

      ctrl.delete(1);
      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'danger', msg: 'No se puede borrar el topic'}]);

      expect(ctrl.topics).toEqualData([
        {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
        {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('TopicListCtrl test create ok del servidor, dando descripcion y sin especificar TTL', function () {
    var scope, ctrl, ctrl2, $httpBackend;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.expect('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
          {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('TopicListCtrl', {$scope: scope});
      ctrl2 = $controller('ModalInstanceCtrl', {$scope: scope});
    }));

    it('debe tener el objeto de la lista con 3 topic y un alert', function () {
      expect(ctrl.topics).toEqualData(undefined);
      expect(ctrl.alerts).toEqualData([]);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([]);

      expect(ctrl.topics).toEqualData([
        {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
        {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      $httpBackend.resetExpectations();

      $httpBackend.expect('POST', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [204, {}];
      });

      $httpBackend.expect('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
          {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'},
          {id: 3, name: 'Tercer topic', description: 'Descripción del tercer topic'}
        ], numberOfElements: 3, totalElements: 3}];
      });

      ctrl2.ok('Tercer topic', 'Descripción del tercer topic', -1);
      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'success', msg: 'Se ha creado un nuevo topic'}]);

      expect(ctrl.topics).toEqualData([
        {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
        {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'},
        {id: 3, name: 'Tercer topic', description: 'Descripción del tercer topic'}
      ]);
    });
  });

  describe('TopicListCtrl test create no hecho del servidor, sin dar descripcion y especificando TTL', function () {
    var scope, ctrl, ctrl2, $httpBackend,
        date = new Date(2016, 9, 20, 10, 0, 0, 0);

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.expect('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
          {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('TopicListCtrl', {$scope: scope});
      ctrl2 = $controller('ModalInstanceCtrl', {$scope: scope});
    }));

    it('debe tener el objeto de la lista con 2 topic y un alert', function () {
      expect(ctrl.topics).toEqualData(undefined);
      expect(ctrl.alerts).toEqualData([]);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([]);

      expect(ctrl.topics).toEqualData([
        {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
        {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      $httpBackend.resetExpectations();

      $httpBackend.expect('POST', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {}];
      });

      ctrl2.ok('Tercer topic', '', date.valueOf());
      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'danger', msg: 'No se ha podido crear el topic'}]);

      expect(ctrl.topics).toEqualData([
        {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
        {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('TopicListCtrl test create no hecho por no tener titulo', function () {
    var scope, ctrl, ctrl2, $httpBackend,
        date = new Date(2016, 9, 20, 10, 0, 0, 0);

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.expect('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
          {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('TopicListCtrl', {$scope: scope});
      ctrl2 = $controller('ModalInstanceCtrl', {$scope: scope});
    }));

    it('debe tener el objeto de la lista con 2 topic y un alert por no tener titulo', function () {
      expect(ctrl.topics).toEqualData(undefined);
      expect(ctrl.alerts).toEqualData([]);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([]);

      expect(ctrl.topics).toEqualData([
        {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
        {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      ctrl2.ok('', '', date.valueOf());

      expect(ctrl.alerts).toEqualData([{type: 'danger', msg: 'No se puede crear el topic sin un titulo'}]);

      expect(ctrl.topics).toEqualData([
        {id: 1, name: 'Primer topic', description: 'Descripción del primer topic'},
        {id: 2, name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });
});
