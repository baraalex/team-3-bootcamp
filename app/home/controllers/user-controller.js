(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name home.controller:userCtrl
   *
   * @description
   *
   */

  angular
    .module('userControllers', [])
    .controller('UserListCtrl', ['User', '$window', 'BlobEncapsulation', getUsers]);

  function getUsers(User, $window, BlobEncapsulation) {
    var vm = this;
    vm.users = User.getUsers().query();
    vm.users.$promise.then(function (result) {
      var data = jSONToCSVConvertor(angular.toJson(result));

      vm.url = $window.URL.createObjectURL(BlobEncapsulation.createBlob(data));
      vm.filename = 'users.csv';
    });
  }

  function jSONToCSVConvertor(JSONData) {
    // If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = angular.fromJson(JSONData), CSV = '', row = '', index, i;
    // This loop will extract the label from 1st index of on array
    for (index in arrData[0]) {
      // Now convert each value to string and comma-seprated
      if (arrData[0].hasOwnProperty(index)) {
        row += index + ',';
      }
    }

    row = row.slice(0, -1);

    // Append label row with line break
    CSV += row + '\r\n';

    // 1st loop is to extract each row
    for (i = 0; i < arrData.length; i++) {
      row = '';
      // 2nd loop will extract each column and convert it in string comma-seprated
      for (index in arrData[i]) {
        if (arrData[0].hasOwnProperty(index)) {
          if (index === 'creationDate') {
            var date = parseDate(arrData[i][index]);
            row += '"' + date + '",';
          } else {
            row += '"' + arrData[i][index] + '",';
          }
        }
      }
      row.slice(0, row.length - 1);
      // add a line break after each row
      CSV += row + '\r\n';
    }

    return CSV;
  }

  function parseDate(date) {
    var d = new Date(date),
        day = d.getDate(),
        month = d.getMonth(),
        year = d.getFullYear(),
        hours = d.getHours(),
        minutes = (d.getMinutes() < 10 ? '0' : '') + d.getMinutes(),
        seconds = d.getSeconds(),
        str = day + '/' + month + '/' + year + ' - ' + hours + ':' + minutes + ':' + seconds;
    return  str;
  }
}());
