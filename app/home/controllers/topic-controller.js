(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name home.controller:TopicCtrl
   *
   * @description
   *
   */
  var vm;

  angular
    .module('topicControllers', ['angularUtils.directives.dirPagination'])
    .controller('TopicListCtrl', ['Topic', showTopics]);

  function getContent(num, Topic) {
    Topic.getTopics(num).query('', function (response) {
      if (response.$status !== 200) {
        vm.topics = '';
        vm.alerts = [];
        vm.alerts.push({type: 'danger', msg: 'No se pueden recuperar los topics del servidor.\nProfavor intentalo mas tarde'});
      } else {
        vm.topics = response.content;
      }
    });
  }

  function getAllTopics(response, Topic) {
    if (response.$status !== 200) {
      vm.alerts = [];
      vm.alerts.push({type: 'danger', msg: 'No se pueden recuperar los topics del servidor.\nProfavor intentalo mas tarde'});
    } else {
      vm.topics = response.content;
      if (response.numberOfElements < response.totalElements) {
        getContent(response.totalElements, Topic);
      }
    }
  }

  function showTopics(Topic) {
    vm = this;
    vm.alerts = [];
    vm.orderByField = 'name';
    vm.reverseSort = false;

    Topic.getTopics().query('', function (response) {
      getAllTopics(response, Topic);
    });

    vm.delete = function (id) {
      Topic.deleteTopic(id).delete('', function (response) {
        if (response.$status === 204) {
          vm.alerts = [];
          vm.alerts.push({type: 'warning', msg: 'Topic borrado'});
          Topic.getTopics().query('', function (resp) {
            getAllTopics(resp, Topic);
          });
        } else {
          vm.alerts = [];
          vm.alerts.push({type: 'danger', msg: 'No se puede borrar el topic'});
        }
      });
    };

    vm.closeAlert = function (index) {
      vm.alerts.splice(index, 1);
    };
  }

  angular
    .module('topicControllers')
    .controller('ModalInstanceCtrl', ['Topic', createTopic]);

  function createTopic(Topic) {
    var vm2 = this, desc, time, data;
    vm2.ok = function (title, description, TimeToLive) {
      if (angular.isDefined(title) && title !== null && title.length > 0) {
        desc = angular.isDefined(description) && description.length > 0 ? description : '';
        time = angular.isDefined(TimeToLive) && TimeToLive > 0 ? TimeToLive : 1;
        data = {
          name: title,
          description: desc,
          ttl: time
        };

        Topic.createTopic().save(data, function (response) {
          if (response.$status === 204) {
            vm.alerts = [];
            vm.alerts.push({type: 'success', msg: 'Se ha creado un nuevo topic'});
            Topic.getTopics().query('', function (resp) {
              getAllTopics(resp, Topic);
            });
          }else {
            vm.alerts = [];
            vm.alerts.push({type: 'danger', msg: 'No se ha podido crear el topic'});
          }
        });
      } else {
        vm.alerts = [];
        vm.alerts.push({type: 'danger', msg: 'No se puede crear el topic sin un titulo'});
      }
    };
  }
}());
