/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('User controllers', function () {
  beforeEach(function () {
    this.addMatchers({
      toEqualData: function (expected) {
        return angular.equals(this.actual, expected);
      }
    });
  });

  beforeEach(module('notificationsApp'));
  beforeEach(module('userServices'));

  describe('UserListCtrl obteniendo un objeto de JSON del servidor', function () {
    var scope, ctrl, $httpBackend, windowMock, blobEncapsulation;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      blobEncapsulation = {
        createBlob: function() {
          return;
        }
      }

      windowMock = {
        URL: {
          createObjectURL: function (data) {
            return 'url';
          }
        }
      };

      $httpBackend = _$httpBackend_;
      $httpBackend.expectGET('/notif/admin/equipo3/users').
        respond([
          {userId: '1', email: 'name@medianet.es', creationDate: 1445450391000, android: 1, ios: 0},
          {userId: '2', email: 'name@medianet.es', creationDate: 1445450391000, android: 1, ios: 0}
        ]);
      scope = $rootScope.$new();
      ctrl = $controller('UserListCtrl', {$scope: scope, $window: windowMock, BlobEncapsulation: blobEncapsulation});
    }));

    it('should show list with 2 users', function () {
      expect(ctrl.users).toEqualData([]);

      $httpBackend.flush();

      expect(ctrl.users).toEqualData([
        {userId: '1', email: 'name@medianet.es', creationDate: 1445450391000, android: 1, ios: 0},
        {userId: '2', email: 'name@medianet.es', creationDate: 1445450391000, android: 1, ios: 0}
      ]);
    });
  });

  describe('UserListCtrl obteniendo un string de JSON del servidor', function () {
    var scope, ctrl, $httpBackend, windowMock, string = [
      {userId: '1', email: 'name@medianet.es', creationDate: 1445450391000, android: 1, ios: 0},
      {userId: '2', email: 'name@medianet.es', creationDate: 1445450391000, android: 1, ios: 0}
        ];

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      windowMock = {
        URL: {
          createObjectURL: function () {
            return 'blob: url-data';
          }
        }
      };

      $httpBackend = _$httpBackend_;
      $httpBackend.expectGET('/notif/admin/equipo3/users').
        respond(string);

      scope = $rootScope.$new();
      ctrl = $controller('UserListCtrl', {$scope: scope, $window: windowMock});
    }));

    xit('should show list with 2 users', function () {
      expect(ctrl.users).toEqualData([]);
      $httpBackend.flush();

      expect(ctrl.users).toEqualData(string);
    });
  });
});
