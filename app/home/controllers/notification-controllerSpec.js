/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('Notification controllers', function () {
  beforeEach(function () {
    this.addMatchers({
      toEqualData: function (expected) {
        return angular.equals(this.actual, expected);
      }
    });
  });

  beforeEach(module('notificationsApp'));
  beforeEach(module('notificationServices'));

  describe('NotificationListCtrl test devolviendo bien la llamada de listar notificaciones con solo 1 llamada a topics', function () {
    var scope, ctrl, ctrl2, $httpBackend;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 2, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
        ]}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('NotificationListCtrl', {$scope: scope});
      ctrl2 = $controller('NotificationSendToUsersCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 notificaciones', function () {
      expect(ctrl.list).toEqualData({});

      $httpBackend.flush();

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('NotificationListCtrl test devolviendo bien la llamada de listar notificaciones con 2 llamadas a topics', function () {
    var scope, ctrl, ctrl2, $httpBackend;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 2, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
        ]}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 3}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics?size=3')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('NotificationListCtrl', {$scope: scope});
      ctrl2 = $controller('NotificationSendToUsersCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 notificaciones', function () {
      expect(ctrl.list).toEqualData({});

      $httpBackend.flush();

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('NotificationListCtrl test devolviendo bien la llamada de listar notificaciones y fallando la 1ª llamada a topics', function () {
    var scope, ctrl, ctrl2, $httpBackend;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 2, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
        ]}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [205, {}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('NotificationListCtrl', {$scope: scope});
      ctrl2 = $controller('NotificationSendToUsersCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 notificaciones', function () {
      expect(ctrl.list).toEqualData({});

      $httpBackend.flush();

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData(undefined);
      expect(ctrl.alerts).toEqualData([{type: 'danger', msg: 'No se pueden recuperar los topics del servidor.\nProfavor intentalo mas tarde'}]);
    });
  });

  describe('NotificationListCtrl test fallando la llamada de listar notificaciones y fallando la 2ª llamada a topics', function () {
    var scope, ctrl, ctrl2, $httpBackend;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [205, {}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 3}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics?size=3')
      .respond(function () {
        return [210, {}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('NotificationListCtrl', {$scope: scope});
      ctrl2 = $controller('NotificationSendToUsersCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 0 notificaciones', function () {
      expect(ctrl.list).toEqualData({});

      $httpBackend.flush();

      expect(ctrl.list.notifications).toEqualData(undefined);

      expect(ctrl2.topics).toEqualData('');
      expect(ctrl.alerts).toEqualData([{type: 'danger', msg: 'No se pueden recuperar los topics del servidor.\nProfavor intentalo mas tarde'}]);

      ctrl.closeAlert(0);
      expect(ctrl.alerts).toEqualData([]);
    });
  });

  describe('NotificationListCtrl test borrar notif ok', function () {
    var scope, ctrl, ctrl2, $httpBackend;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 2, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
        ]}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('NotificationListCtrl', {$scope: scope});
      ctrl2 = $controller('NotificationSendToUsersCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 notificaciones', function () {
      expect(ctrl.list).toEqualData({});

      $httpBackend.flush();

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      $httpBackend.resetExpectations();

      $httpBackend.expect('DELETE', '/notif/admin/equipo3/notifications/1')
      .respond(function () {
        return [204, {}];
      });

      $httpBackend.expect('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 2, notifications: [
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
        ]}];
      });

      ctrl.delete(1);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'warning', msg: 'Notificacion borrada'}]);

      expect(ctrl.list.notifications).toEqualData([
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('NotificationListCtrl test borrar notifi mal', function () {
    var scope, ctrl, ctrl2, $httpBackend;

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 2, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
        ]}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('NotificationListCtrl', {$scope: scope});
      ctrl2 = $controller('NotificationSendToUsersCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 notificaciones', function () {
      expect(ctrl.list).toEqualData({});

      $httpBackend.flush();

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      $httpBackend.resetExpectations();

      $httpBackend.expect('DELETE', '/notif/admin/equipo3/notifications/1')
      .respond(function () {
        return [200, {}];
      });

      ctrl.delete(1);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'danger', msg: 'No se puede borrar la Notificacion'}]);

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('NotificationListCtrl test enviar notif All ok', function () {
    var scope, ctrl, ctrl2, $httpBackend,
        date = new Date(2016, 9, 20, 10, 0, 0, 0);

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 2, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
        ]}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('NotificationListCtrl', {$scope: scope});
      ctrl2 = $controller('NotificationSendToUsersCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 notificaciones', function () {
      expect(ctrl.list).toEqualData({});

      $httpBackend.flush();

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      $httpBackend.resetExpectations();

      $httpBackend.expect('POST', '/notif/admin/equipo3/notifications')
      .respond(function () {
        return [204, {}];
      });

      $httpBackend.expect('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 3, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000},
          {id: 3, title: 'Notif 3', content: 'Contenido de Notif 3', sendDate: date.valueOf()}
        ]}];
      });

      ctrl2.send(undefined, true, 'Notif 3', 'Contenido de Notif 3', undefined, undefined, undefined);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'success', msg: 'Se ha creado una nueva notificacion'}]);

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000},
        {id: 3, title: 'Notif 3', content: 'Contenido de Notif 3', sendDate: date.valueOf()}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('NotificationListCtrl test enviar notif All mal servidor no responde 204', function () {
    var scope, ctrl, ctrl2, $httpBackend,
        date = new Date(2016, 9, 20, 10, 0, 0, 0);

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 2, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
        ]}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('NotificationListCtrl', {$scope: scope});
      ctrl2 = $controller('NotificationSendToUsersCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 notificaciones', function () {
      expect(ctrl.list).toEqualData({});

      $httpBackend.flush();

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      $httpBackend.resetExpectations();

      $httpBackend.expect('POST', '/notif/admin/equipo3/notifications')
      .respond(function () {
        return [200, {}];
      });

      ctrl2.send('', true, 'Notif 3', 'Contenido de Notif 3', date, date, '');

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'danger', msg: 'No se puede enviar la Notificacion'}]);

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('NotificationListCtrl test borrar notif mal por no haber titulo (esto vale para todos los envios)', function () {
    var scope, ctrl, ctrl2, $httpBackend,
        date = new Date(2016, 9, 20, 10, 0, 0, 0);

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 2, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
        ]}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('NotificationListCtrl', {$scope: scope});
      ctrl2 = $controller('NotificationSendToUsersCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 notificaciones', function () {
      expect(ctrl.list).toEqualData({});

      $httpBackend.flush();

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      ctrl2.send('', true, '', 'Contenido de Notif 3', date, date, '');

      expect(ctrl.alerts).toEqualData([{type: 'danger', msg: 'No hay titulo en la notificacion.\nProfavor rellena el campo'}]);

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('NotificationListCtrl test enviar notif 1 usuario ok para mas adelante', function () {
    var scope, ctrl, ctrl2, $httpBackend,
        date = new Date(2016, 9, 20, 10, 0, 0, 0);

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 2, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
        ]}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('NotificationListCtrl', {$scope: scope});
      ctrl2 = $controller('NotificationSendToUsersCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 notificaciones', function () {
      expect(ctrl.list).toEqualData({});

      $httpBackend.flush();

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      $httpBackend.resetExpectations();

      $httpBackend.expect('POST', '/notif/admin/equipo3/notifications')
      .respond(function () {
        return [204, {}];
      });

      $httpBackend.expect('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 3, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000},
          {id: 3, title: 'Notif 3', content: 'Contenido de Notif 3', sendDate: date.valueOf()}
        ]}];
      });

      ctrl2.send('team 3', false, 'Notif 3', 'Contenido de Notif 3', date, date, undefined);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'success', msg: 'Se ha creado una nueva notificacion'}]);

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000},
        {id: 3, title: 'Notif 3', content: 'Contenido de Notif 3', sendDate: date.valueOf()}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('NotificationListCtrl test enviar notif 1 usuario servidor responde distinto de 204', function () {
    var scope, ctrl, ctrl2, $httpBackend,
        date = new Date(2016, 9, 20, 10, 0, 0, 0);

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 2, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
        ]}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('NotificationListCtrl', {$scope: scope});
      ctrl2 = $controller('NotificationSendToUsersCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 notificaciones', function () {
      expect(ctrl.list).toEqualData({});

      $httpBackend.flush();

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      $httpBackend.resetExpectations();

      $httpBackend.expect('POST', '/notif/admin/equipo3/notifications')
      .respond(function () {
        return [200, {}];
      });

      ctrl2.send('team 3', false, 'Notif 3', 'Contenido de Notif 3', date, date, undefined);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'danger', msg: 'No se puede enviar la Notificacion'}]);

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('NotificationListCtrl test enviar notif topic ok y ahora', function () {
    var scope, ctrl, ctrl2, $httpBackend,
        date = new Date();

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 2, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
        ]}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('NotificationListCtrl', {$scope: scope});
      ctrl2 = $controller('NotificationSendToUsersCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 notificaciones', function () {
      expect(ctrl.list).toEqualData({});

      $httpBackend.flush();

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      $httpBackend.resetExpectations();

      $httpBackend.expect('POST', '/notif/admin/equipo3/notifications')
      .respond(function () {
        return [204, {}];
      });

      $httpBackend.expect('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 3, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000},
          {id: 3, title: 'Notif 3', content: 'Contenido de Notif 3', sendDate: date.valueOf()}
        ]}];
      });

      ctrl2.send('', false, 'Notif 3', 'Contenido de Notif 3', undefined, undefined, '163');

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'success', msg: 'Se ha creado una nueva notificacion'}]);

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000},
        {id: 3, title: 'Notif 3', content: 'Contenido de Notif 3', sendDate: date.valueOf()}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('NotificationListCtrl test enviar notif topic servidor responde distinto de 204', function () {
    var scope, ctrl, ctrl2, $httpBackend,
        date = new Date(2016, 9, 20, 10, 0, 0, 0);

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 2, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
        ]}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('NotificationListCtrl', {$scope: scope});
      ctrl2 = $controller('NotificationSendToUsersCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 notificaciones', function () {
      expect(ctrl.list).toEqualData({});

      $httpBackend.flush();

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      $httpBackend.resetExpectations();

      $httpBackend.expect('POST', '/notif/admin/equipo3/notifications')
      .respond(function () {
        return [200, {}];
      });

      ctrl2.send('', false, 'Notif 3', 'Contenido de Notif 3', date, date, '163');

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'danger', msg: 'No se puede enviar la Notificacion'}]);

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });

  describe('NotificationListCtrl test enviar notif 1 usuario ok para ahora', function () {
    var scope, ctrl, ctrl2, $httpBackend,
        date = new Date();

    beforeEach(inject(function (_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;

      $httpBackend.when('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 2, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
        ]}];
      });

      $httpBackend.when('GET', '/notif/admin/equipo3/topics')
      .respond(function () {
        return [200, {content:[
          {name: 'Primer topic', description: 'Descripción del primer topic'},
          {name: 'Segundo topic', description: 'Descripción del segundo topic'}
        ], numberOfElements: 2, totalElements: 2}];
      });

      scope = $rootScope.$new();
      ctrl = $controller('NotificationListCtrl', {$scope: scope});
      ctrl2 = $controller('NotificationSendToUsersCtrl', {$scope: scope});
    }));

    it('debe mostrar una lista con 2 notificaciones', function () {
      expect(ctrl.list).toEqualData({});

      $httpBackend.flush();

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);

      $httpBackend.resetExpectations();

      $httpBackend.expect('POST', '/notif/admin/equipo3/notifications')
      .respond(function () {
        return [204, {}];
      });

      $httpBackend.expect('GET', '/notif/admin/equipo3/notifications/pending')
      .respond(function () {
        return [200, {total: 3, notifications: [
          {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
          {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000},
          {id: 3, title: 'Notif 3', content: 'Contenido de Notif 3', sendDate: date.valueOf()}
        ]}];
      });

      ctrl2.send('team 3', false, 'Notif 3', 'Contenido de Notif 3', undefined, undefined, undefined);

      $httpBackend.flush();

      expect(ctrl.alerts).toEqualData([{type: 'success', msg: 'Se ha creado una nueva notificacion'}]);

      expect(ctrl.list.notifications).toEqualData([
        {id: 1, title: 'Notif 1', content: 'Contenido de Notif 1', sendDate: 1455730649000},
        {id: 2, title: 'Notif 2', content: 'Contenido de Notif 2', sendDate: 1477743300000},
        {id: 3, title: 'Notif 3', content: 'Contenido de Notif 3', sendDate: date.valueOf()}
      ]);

      expect(ctrl2.topics).toEqualData([
        {name: 'Primer topic', description: 'Descripción del primer topic'},
        {name: 'Segundo topic', description: 'Descripción del segundo topic'}
      ]);
    });
  });
});
