(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name home.controller:NotificationCtrl
   *
   * @description
   *
   */

  var vm;

  angular
    .module('notificationControllers', ['angularUtils.directives.dirPagination'])
    .controller('NotificationListCtrl', ['Notification', showNotifications]);

  function showNotifications(Notification) {
    vm = this;
    vm.list = Notification.getNotifications().query();

    vm.closeAlert = function (index) {
      vm.alerts.splice(index, 1);
    };
    vm.delete = function (id) {
      Notification.deleteNotification(id).delete('', function (response) {
        if (response.$status === 204) {
          vm.alerts = [];
          vm.alerts.push({type: 'warning', msg: 'Notificacion borrada'});
          vm.list = Notification.getNotifications().query();
        } else {
          vm.alerts = [];
          vm.alerts.push({type: 'danger', msg: 'No se puede borrar la Notificacion'});
        }
      });
    };
  }

  angular
    .module('notificationControllers')
    .controller('NotificationSendToUsersCtrl', ['Notification', 'Topic', sendTo]);

  function sendTo(Notification, Topic) {
    var vm2 = this;
    vm2.today = getCurrentDate();
    vm2.future = getMaxDate();
    vm.alerts = [];
    vm.orderByField = 'sendDate';
    vm.reverseSort = false;

    Topic.getTopics().query('', function (response) {
      getAllTopics(response, Topic);
    });

    vm2.send = function (to, all, title, content, date, time, topic) {
      if (angular.isDefined(title) && title !== null && title.length > 0) {
        if (all) {
          sendAll(Notification, title, content, date, time);
        } else {
          if (angular.isDefined(to) && to.length > 0) {
            sendUsers(Notification, to, title, content, date, time);
          }
          if (angular.isDefined(topic) && topic !== '-1') {
            sendToTopic(Notification, topic, title, content, date, time);
          }
        }
      } else {
        vm.alerts = [];
        vm.alerts.push({type: 'danger', msg: 'No hay titulo en la notificacion.\nProfavor rellena el campo'});
      }
    };

    function getContent(num) {
      Topic.getTopics(num).query('', function (response) {
        if (response.$status !== 200) {
          vm2.topics = '';
          vm.alerts = [];
          vm.alerts.push({type: 'danger', msg: 'No se pueden recuperar los topics del servidor.\nProfavor intentalo mas tarde'});
        } else {
          vm2.topics = response.content;
        }
      });
    }

    function getAllTopics(response) {
      if (response.$status !== 200) {
        vm.alerts = [];
        vm.alerts.push({type: 'danger', msg: 'No se pueden recuperar los topics del servidor.\nProfavor intentalo mas tarde'});
      } else {
        vm2.topics = response.content;
        if (response.numberOfElements < response.totalElements) {
          getContent(response.totalElements, Topic);
        }
      }
    }
  }

  function sendAll(Notification, title, content, date, time) {
    var data = makeRequestSendAll(title, content, date, time);
    Notification.sendToUsers().save(data, function (response) {
      if (response.$status === 204) {
        vm.alerts = [];
        vm.alerts.push({type: 'success', msg: 'Se ha creado una nueva notificacion'});
        vm.list = Notification.getNotifications().query();
      } else {
        vm.alerts = [];
        vm.alerts.push({type: 'danger', msg: 'No se puede enviar la Notificacion'});
      }
    });
  }

  function sendUsers(Notification, to, title, content, date, time) {
    var data = makeRequestSendUsers(to, title, content, date, time);
    Notification.sendToUsers().save(data, function (response) {
      if (response.$status === 204) {
        vm.alerts = [];
        vm.alerts.push({type: 'success', msg: 'Se ha creado una nueva notificacion'});
        vm.list = Notification.getNotifications().query();
      } else {
        vm.alerts = [];
        vm.alerts.push({type: 'danger', msg: 'No se puede enviar la Notificacion'});
      }
    });
  }

  function sendToTopic(Notification, id, title, content, date, time) {
    var data = makeRequestSendTopic(id, title, content, date, time);
    Notification.sendToUsers().save(data, function (response) {
      if (response.$status === 204) {
        vm.alerts = [];
        vm.alerts.push({type: 'success', msg: 'Se ha creado una nueva notificacion'});
        vm.list = Notification.getNotifications().query();
      } else {
        vm.alerts = [];
        vm.alerts.push({type: 'danger', msg: 'No se puede enviar la Notificacion'});
      }
    });
  }

  function makeRequestSendUsers(to, title, content, date, time) {
    var toParsed = to.split(/[ ,]+/),
        data = {
          to: toParsed,
          title: title,
          content: content
        };

    if (date && time) {
      data.sendDate = createDate(date, time);
    }

    return data;
  }

  function makeRequestSendAll(title, content, date, time) {
    var data = {
      toAll: true,
      title: title,
      content: content
    };

    if (date && time) {
      data.sendDate = createDate(date, time);
    }

    return data;
  }

  function makeRequestSendTopic(id, title, content, date, time) {
    var data = {
      toTopic: id,
      title: title,
      content: content
    };

    if (date && time) {
      data.sendDate = createDate(date, time);
    }

    return data;
  }

  function parseDate(date) {
    var y = date.getFullYear(),
        m = date.getMonth(),
        d = date.getDate();

    return {year: y, month: m, day: d};
  }

  function parseTime(time) {
    var h = time.getHours(),
        m = time.getMinutes();

    return {hours: h, minutes: m};
  }

  function createDate(date, time) {
    var d = parseDate(date),
        t = parseTime(time);

    return new Date(d.year, d.month, d.day, t.hours, t.minutes).valueOf();
  }

  function getCurrentDate() {
    return new Date().toISOString().split('T')[0];
  }

  function getMaxDate() {
    var date = new Date();
    date.setDate(date.getDate() + 6);
    return date.toISOString().split('T')[0];
  }
}());
