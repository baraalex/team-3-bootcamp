var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var app = express();
var router = express.Router();

var seconds = 1 * 1000;

var config = require('./configServer.json');

var URLbase = config.URLbase;
var token = config.token;

app.set('port', process.env.PORT || config.port);

app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users

app.listen(app.get('port'), function () {
  console.log('Express up and listening on port ' + app.get('port'));
});

// ADMIN - Topics
app.route('/notif/admin/equipo3/topics')
  // listTopics
  .get(function (req, res) {

    var options = {
      host: URLbase,
      path: req.originalUrl,
      port: '9090',
      headers: {'content-type': 'application/json', 'Application-Authorization': token}
    };

    callback = function(response) {
      var str = ''
      response.on('data', function (chunk) {
        str += chunk;
      });

      response.on('end', function () {
        objeto = JSON.parse(str);
        res.json(objeto);
        response.setEncoding('utf8');
        code = response.statusCode;
        res.status(code).end();
      });
    }
    var req2 = http.get(options, callback);

    // req2.on('socket', function (socket) {
    //   socket.setTimeout(seconds);
    //   socket.on('timeout', function() {
    //     console.log("timeout llegado en get");
    //     req2.end();
    //     res.status(404).end();
    //   });
    // });
    req2.end();

  })
  // createTopic
  .post(function (req, res) {

    var data = JSON.stringify(req.body);
    var options = {
      host: URLbase,
      path: '/notif/admin/equipo3/topics',
      port: '9090',
      method:'POST',
      headers: {'content-type': 'application/json', 'Application-Authorization': token}
    };
    var code = 404;


    var req2 = http.request(options , function(response) {
      response.setEncoding('utf8');
      code = response.statusCode;
      res.status(code).end();
    });

    // req2.on('socket', function (socket) {
    //   socket.setTimeout(seconds);
    //   socket.on('timeout', function() {
    //     console.log("timeout llegado en post");
    //     req2.end();
    //     res.status(404).end();
    //   });
    // });

    req2.write(data);
    req2.end();
  });

// delete Topics
app.delete('/notif/admin/equipo3/topics/:topic_id',function (req, res) {

    var options = {
      host: URLbase,
      path: req.originalUrl,
      port: '9090',
      method: 'DELETE',
      headers: {'content-type': 'application/json', 'Application-Authorization': token}
    };

    var code = 404;

    var req2 = http.request(options , function(response) {
      response.setEncoding('utf8');
      code = response.statusCode;
      res.status(code).end();
    });

    // req2.on('socket', function (socket) {
    //   socket.setTimeout(seconds);
    //   socket.on('timeout', function() {
    //     console.log("timeout llegado en post");
    //     req2.end();
    //     res.status(404).end();
    //   });
    // });

    req2.end();
  });

// ADMIN - Users
app.route('/notif/admin/equipo3/users')
  // listUsers
  .get(function (req, res) {

    var options = {
      host: URLbase,
      path: '/notif/admin/equipo3/users',
      port: '9090',
      headers: {'content-type': 'application/json', 'Application-Authorization': token}
    };

    callback = function(response) {
      var str = ''
      response.on('data', function (chunk) {
        str += chunk;
      });

      response.on('end', function () {
        objeto = JSON.parse(str);
        res.json(objeto);
      });
    }
    var req2 = http.get(options, callback);

    // req2.on('socket', function (socket) {
    //   socket.setTimeout(seconds);
    //   socket.on('timeout', function() {
    //     console.log("timeout llegado en users");
    //     req2.end();
    //     res.status(404).end();
    //   });
    // });

    req2.end();

  });

// ADMIN - Notification
app.route('/notif/admin/equipo3/notifications/pending')
  // get pending
  .get(function (req, res) {

    var options = {
      host: URLbase,
      path: '/notif/admin/equipo3/notifications/pending',
      port: '9090',
      headers: {'content-type': 'application/json', 'Application-Authorization': token}
    };

    callback = function(response) {
      var str = ''
      response.on('data', function (chunk) {
        str += chunk;
      });

      response.on('end', function () {
        objeto = JSON.parse(str);
        res.json(objeto);
      });
    }
    var req2 = http.get(options, callback);

    // req2.on('socket', function (socket) {
    //   socket.setTimeout(seconds);
    //   socket.on('timeout', function() {
    //     console.log("timeout llegado en get");
    //     req2.end();
    //     res.status(404).end();
    //   });
    // });
    req2.end();

  });

// ADMIN - Notification
app.route('/notif/admin/equipo3/notifications')
  // SendToUsers
  .post(function (req, res) {

    var data = JSON.stringify(req.body);
    var options = {
      host: URLbase,
      path: '/notif/admin/equipo3/notifications',
      port: '9090',
      method:'POST',
      headers: {'content-type': 'application/json', 'Application-Authorization': token}
    };
    var code = 404;

    var req2 = http.request(options , function(response) {
      response.setEncoding('utf8');
      code = response.statusCode;
      res.status(code).end();
    });

    req2.write(data);
    req2.end();
  });

// delete Notificacion
app.delete('/notif/admin/equipo3/notifications/:notif_id',function (req, res) {

    var options = {
      host: URLbase,
      path: req.originalUrl,
      port: '9090',
      method: 'DELETE',
      headers: {'content-type': 'application/json', 'Application-Authorization': token}
    };

    var code = 404;

    var req2 = http.request(options , function(response) {
      response.setEncoding('utf8');
      code = response.statusCode;
      res.status(code).end();
    });

    // req2.on('socket', function (socket) {
    //   socket.setTimeout(seconds);
    //   socket.on('timeout', function() {
    //     console.log("timeout llegado en post");
    //     req2.end();
    //     res.status(404).end();
    //   });
    // });

    req2.end();
  });
