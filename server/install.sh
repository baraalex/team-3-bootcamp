#!/bin/sh

mkdir Team3Bootcamp
tar -zxvf server.tar.gz -C Team3Bootcamp
cd Team3Bootcamp
#install dependencies
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs
#install server itself
npm install
node server.js
